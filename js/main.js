window.addEventListener("load", event => {
	// console.log("loaded");
	main();
});

const main = event => {
	
	console.log("MAIN");
	let mainCanvas = document.createElement("canvas");
	let ctx = mainCanvas.getContext('2d');
	let img = new Image();
	let imgSwitch = false;
	document.body.appendChild(mainCanvas);
	mainCanvas.width = 745;
	mainCanvas.height = 745;
	img.onload = function() {
		ctx.drawImage(img, 0, 0);
	}
	mainCanvas.addEventListener('click', function(event) {
		console.log("clicked");
		if(imgSwitch) img.src = 'img/img1.jpg';
		else img.src = 'img/img2.jpg';
		imgSwitch = !imgSwitch;
		ctx.drawImage(img, 0, 0);
	});
	img.src = 'img/img1.jpg';

}